package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String, Double> values = new HashMap<String, Double>();

    public double getBookPrice(String isbn) {
        switch (isbn){
            case "1": return 10.0;
            case "2": return 45.0;
            case "3": return 20.0;
            case "4": return 35.0;
            case "5": return 50.0;
            case "others": return 0.0;
        }
        return 0;
    }
}
